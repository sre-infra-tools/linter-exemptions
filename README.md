# Usage

This slide deck is part of the [_linter_exemptions_](https://extranet.atlassian.com/display/PSRE/Infra+SRE+Demos) InfraSRE demos.

# Installation

1. Clone this code into a directory
 ```
 git clone git@bitbucket.org:sre-infra-tools/linter-exemptions.git
 ```

2. Install the Go present tool
 ```
 go get -u -v golang.org/x/tools/cmd/present
 ```

3. Run the present tool
 ```
 cd linter-exemptions && present
 ```

The slides will be available at [http://127.0.0.1:3999/linter-exemptions.slide](http://127.0.0.1:3999/linter-exemptions.slide#1)

# Online
You can view current version of slides at [https://statlas.prod.atl-paas.net/linter-exemptions/linter-exemptions.slide#1](https://statlas.prod.atl-paas.net/linter-exemptions/linter-exemptions.slide#1)

# License and Materials

This presentation is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) licence.

You are encouraged to remix, transform, or build upon the material, providing you give appropriate credit and distribute your contributions under the same license.
